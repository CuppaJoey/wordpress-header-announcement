<?php
  defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

  /*
   *  Plugin Updater Class
   *
   *  @version 1.0.0
   *  @author Joseph Schultz
   */

  class cjUpdater {
    public $version;
    public $slug;

    function __construct() {

    }

    public function set_version($version) {
      $this->version = $version;
    }

    public function set_slug($slug) {
      $this->slug = $slug;
    }

    public function init($version, $slug) {
      $this->set_version($version);
      $this->set_slug($slug);

      add_filter('plugins_api', array($this, 'get_plugin_info'), 10, 3);
      add_filter('site_transient_update_plugins', array($this, 'push_update'));
      add_action('upgrader_process_complete', array($this, 'delete_transient'), 10, 2);
    }

    /*
     *  Get plugin info from server json file
     *  Necessary for receiving updates
     *
     */
    public function get_plugin_info($res, $action, $args) {
      // do nothing if this is not about getting plugin information
      if ('plugin_information' !== $action) {
        return false;
      }

      $plugin_slug = $this->slug; // we are going to use it in many places in this function

      // do nothing if it is not our plugin
      if( $plugin_slug !== $args->slug ) {
        return false;
      }

      // trying to get from cache first
      if( false == $remote = get_transient( 'cuppajoey_update_' . $plugin_slug ) ) {

        // info.json is the file with the actual plugin information on your server
        $remote = wp_remote_get( 'https://plugins.cuppajoey.com/'.$plugin_slug.'/info.json', array(
          'timeout' => 10,
          'headers' => array(
            'Accept' => 'application/json'
          ) )
        );

        if ( ! is_wp_error($remote) && isset($remote['response']['code']) && $remote['response']['code'] == 200 && ! empty($remote['body']) ) {
          set_transient('cuppajoey_update_' . $plugin_slug, $remote, 43200); // 12 hours cache
        }

      }

      if( ! is_wp_error($remote) && isset($remote['response']['code']) && $remote['response']['code'] == 200 && ! empty($remote['body']) ) {

        $remote = json_decode($remote['body']);
        $res = new stdClass();

        $res->name = $remote->name;
        $res->slug = $plugin_slug;
        $res->version = $remote->version;
        $res->tested = $remote->tested;
        $res->requires = $remote->requires;
        $res->author = '<a href="https://cuppajoey.com">Joey Schultz</a>';
        // $res->author_profile = 'https://profiles.wordpress.org/rudrastyh';
        $res->download_link = $remote->download_url;
        $res->trunk = $remote->download_url;
        $res->requires_php = '5.3';
        $res->last_updated = $remote->last_updated;
        $res->sections = array(
          'description' => $remote->sections->description,
          'installation' => $remote->sections->installation,
          'changelog' => $remote->sections->changelog
          // you can add your custom sections (tabs) here
        );

        // in case you want the screenshots tab, use the following HTML format for its content:
        // <ol><li><a href="IMG_URL" target="_blank"><img src="IMG_URL" alt="CAPTION" /></a><p>CAPTION</p></li></ol>
        if( !empty( $remote->sections->screenshots ) ) {
          $res->sections['screenshots'] = $remote->sections->screenshots;
        }

        $res->banners = array(
          'low' => 'https://plugins.cuppajoey.com/'.$plugin_slug.'/'.$plugin_slug.'-banner-low.jpg',
          'high' => 'https://plugins.cuppajoey.com/'.$plugin_slug.'/'.$plugin_slug.'-banner.jpg'
        );
        return $res;

      }

      return false;
    }


    /*
     *  Push update transient to database
     *
     */
    public function push_update($transient) {
      $plugin_slug = $this->slug;

    	if ( empty($transient->checked) ) {
        return $transient;
      }

    	// trying to get from cache first, to disable cache comment 10,20,21,22,24
    	if( false == $remote = get_transient('cuppajoey_upgrade_'.$plugin_slug) ) {

    		// info.json is the file with the actual plugin information on your server
    		$remote = wp_remote_get( 'https://plugins.cuppajoey.com/'.$plugin_slug.'/info.json', array(
    			'timeout' => 10,
    			'headers' => array(
    				'Accept' => 'application/json'
    			) )
    		);

    		if ( !is_wp_error($remote) && isset($remote['response']['code']) && $remote['response']['code'] == 200 && !empty($remote['body']) ) {
    			set_transient('cuppajoey_upgrade_'.$plugin_slug, $remote, 43200); // 12 hours cache
    		}

    	}

    	if ($remote) {
    		$remote = json_decode($remote['body']);

    		// your installed plugin version should be on the line below! You can obtain it dynamically of course
    		if( $remote && version_compare($this->version, $remote->version, '<') && version_compare($remote->requires, get_bloginfo('version'), '<') ) {
    			$res = new stdClass();
    			$res->slug = $plugin_slug;
    			$res->plugin = $plugin_slug.'/'.$plugin_slug.'.php'; // it could be just YOUR_PLUGIN_SLUG.php if your plugin doesn't have its own directory
    			$res->new_version = $remote->version;
    			$res->tested = $remote->tested;
    			$res->package = $remote->download_url;
         		$transient->response[$res->plugin] = $res;
         		//$transient->checked[$res->plugin] = $remote->version;
        }
    	}

      return $transient;
    }


    /*
     *  Delete transient after update
     *
     */
    public function delete_transient($upgrader_object, $options) {
      $plugin_slug = $this->slug;

    	if ($options['action'] == 'update' && $options['type'] === 'plugin')  {
    		// just clean the cache when new plugin version is installed
    		delete_transient( 'cuppajoey_upgrade_'.$plugin_slug );
    	}
    }


  }
?>
