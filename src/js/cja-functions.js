(function($) {
  $(document).ready(function () {
    if (cjaArgs.excerpt) {
      var output = '<div id="cja_alert">';
        output += '<div id="cja_content">';
          output += '<div id="cja_preview">' + cjaArgs.excerpt + '</div>';
          output += '<div id="cja_reveal">' + cjaArgs.message + '</div>';
        output += '</div>';
        output += '<button id="cja_button">Read More</button>';
      output += '</div>';

      $(output).prependTo('body');
    }

    // Debug Mode
    // Console log all variables
    if (cjaArgs.debug_mode) {
      console.log(cjaArgs);
    }

    cjaToggleHandler();

  });

  function cjaToggleHandler() {
    var toggle = $('#cja_button');
    var tempText = 'Collapse';
    var container = $('#cja_alert');
    var reveal = $('#cja_reveal');
    var height = $('#cja_preview').height();


    container.height(height);

    $(window).resize(function(){
      height = $('#cja_preview').height();

      if (reveal.hasClass('revealed')) {
        container.height(height + reveal.height() + 25);
      } else {
        container.height(height);
      }
    });

    toggle.click(function() {
      if (container.height() > height) {
        container.height(height);
      } else {
        container.height(height + reveal.height() + 25);
      }
      var oldText = toggle.text();
      reveal.toggleClass('revealed');

      toggle.text(tempText);
      tempText = oldText;
    });
  }
})(jQuery);
