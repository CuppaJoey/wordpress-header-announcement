<?php
/*
  Plugin Name: Header Announcement
  Plugin URI: https://cuppajoey.com
  Version: 1.2.0
  Author: Joseph Schultz
  Author URL: https://cuppajoey.com
  Description: Insert an announcement above your website header.
*/

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

// Include updater class
require('includes/class-cuppajoey-updater.php');

// Load scripts & styles
class cjAnnouncement {

  public $version;
  public $plugin_slug;
  public $updater;

  function __construct() {
    $this->version = '1.2.0';
    $this->plugin_slug = 'cj-announcement';
  }

  public function load_scripts_and_styles() {
    wp_register_style('cja-styles', plugins_url('/'.$this->plugin_slug.'/css/cja-style.css'));
    wp_register_script('cja-functions', plugins_url('/'.$this->plugin_slug.'/js/cja-functions.js'), array(), '1.0', true);

    wp_enqueue_style('cja-styles');

    // Set Script parameters
		$args = array(
			// script specific parameters
			'excerpt' => get_option('cja_excerpt'),
			'message' => get_option('cja_message'),
		);

    // Enqueue the script
		wp_localize_script('cja-functions', 'cjaArgs', $args);
    wp_enqueue_script('cja-functions');
  }

  function admin_menu() {
  	add_menu_page('Header Announcement', 'Header Announcement', 'administrator', 'cja-settings', array($this, 'plugin_settings_markup'), 'dashicons-admin-generic');
  }

  function plugin_settings() {
  	register_setting('cja-settings-group', 'cja_excerpt');
  	register_setting('cja-settings-group', 'cja_message');
  }

  function plugin_settings_markup() {
  	?>

  	<div class="wrap">
  		<!-- Settings Form -->
  		<form method="post" action="options.php">
  			<?php settings_fields('cja-settings-group'); ?>
  			<?php do_settings_sections('cja-settings-group'); ?>
  			<table class="form-table">
  				<!-- Text Contents -->
  				<tr valign="top">
  					<th scope="row">
  						Annoucement Preview
  						<br><span style="font-weight:400;">This is the message that appears when announcement is collapsed. (Leaving this blank removes the banner)</span>
  					</th>
						<td>
							<textarea id="cja_excerpt" style="height: 150px;width: 75%;" name="cja_excerpt"><?php echo get_option('cja_excerpt'); ?></textarea>
						</td>
  				</tr>
          <tr valign="top">
  					<th scope="row">
  						Annoucement Message
  						<br><span style="font-weight:400;">This is the rest of the message that appears when announcement is revealed.</span>
  					</th>
						<td>
							<textarea id="cja_message" style="height: 150px;width: 75%;" name="cja_message"><?php echo get_option('cja_message'); ?></textarea>
						</td>
  				</tr>
  			</table>

  			<!-- Save Changes Button -->
  			<?php submit_button(); ?>
  		</form>
  	</div>

  	<?php
  }


  public function run() {
    add_action('wp_enqueue_scripts', array($this, 'load_scripts_and_styles'));
    add_action('admin_menu', array($this,'admin_menu'));
    add_action('admin_init', array($this,'plugin_settings'));
  }

}

$cjaAlert = new cjAnnouncement;
$cjaAlert->run();

$cjaUpdater = new cjUpdater;
$cjaUpdater->init($cjaAlert->version, $cjaAlert->plugin_slug);

?>
