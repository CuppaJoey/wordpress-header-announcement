var gulp = require('gulp');

var paths = {
  src: 'src/**/*',
  srcPHP: 'src/**/*.php',
  srcCSS: 'src/**/*.css',
  srcJS: 'src/**/*.js',
  dist: 'dist',
  distCSS: 'dist/**/*.css',
  distJS: 'dist/**/*.js'
};

gulp.task('default', function () {
  console.log('Hello World!');
});
